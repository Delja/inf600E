
package interpreter;

import java.util.HashMap;
import java.util.Map;

import interpreter.analysis.*;
import interpreter.node.*;

public class Program extends DepthFirstAdapter {

	private Program() {}

	public static void start(Node tree) {
		Program program = new Program();
		tree.apply(program);
	}
	
	
	
}
