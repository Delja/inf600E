# Objectif du laboratoire 3

- Ajouter l'appel de procédure dans notre interpréteur
	- Ajoute les éléments nécessaires dans la grammaire pour prendre en compte la syntaxe suivante:
		```
			fun test(i) {
				[...]
			}
		```
- Analyse sémantique
	- Définir un visiteur `functionFinder` (Pour découvrir l'ensemble des méthodes disponible). Note Il faut aussi définir un classe functionTable pour stocker les méthodes trouvées.
	- Définir un visiteur `CallVerifier`. Ce visiteur a pour objectif de valider le call (Vérifie dans la functionTable si une méthode existe avec ce nom).
- Redéfinir les différentes cases pour prendre en considération la nouvelle syntaxe.
- Définir un système de frame en prenant en compte nos nouvelles problématiques (variable locale, paramètre des procédures...)