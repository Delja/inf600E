package interpreter.value;

import interpreter.ProgramException;
import interpreter.node.Token;

public class IntValue extends Value {
	final Integer value;

	public IntValue(
    		Integer value) {
        this.value = value;
    }

	public Integer getValue() {
		return this.value;
	}

	public Value add(Value value, Token position) {
		if (value instanceof IntValue) {
			return (new IntValue(this.getValue() + ((IntValue) value).getValue()));
		}
		throw new ProgramException(position, "unsupported operand type(s) for +:" + this.to_s() + " + " + value.to_s());
	};

	public Value sub(Value value, Token position) {
		if (value instanceof IntValue) {
			return (new IntValue(this.getValue() - ((IntValue) value).getValue()));
		}
		throw new ProgramException(position, "unsupported operand type(s) for -:" + this.to_s() + " - " + value.to_s());
	};

	public Value mul(Value value, Token position) {
		if (value instanceof IntValue) {
			return (new IntValue(this.getValue() * ((IntValue) value).getValue()));
		}
		throw new ProgramException(position, "unsupported operand type(s) for *:" + this.to_s() + " * " + value.to_s());
	};

	public Value div(Value value, Token position) {
		if (value instanceof IntValue) {
			if(((IntValue) value).getValue() == 0){
				throw new ProgramException(position, "division by 0 is not allowed");
			}
			return (new IntValue(this.getValue() / ((IntValue) value).getValue()));
		}
		throw new ProgramException(position, "unsupported operand type(s) for /:" + this.to_s() + " / " + value.to_s());
	};

	public Value mod(Value value, Token position) {
		if (value instanceof IntValue) {
			return (new IntValue(this.getValue() % ((IntValue) value).getValue()));
		}
		throw new ProgramException(position, "unsupported operand type(s) for %:" + this.to_s() + " % " + value.to_s());
	};

	public Boolean greaterThan(Value value, Token position) {
		if (value instanceof IntValue) {
			if (this.getValue() > ((IntValue) value).getValue()) {
				return true;
			} else {
				return false;
			}
		}
		throw new ProgramException(position, "unsupported operand type(s) for >:" + this.to_s() + " > " + value.to_s());
	}

	public String to_s() {
		return value.toString();
	};
}
