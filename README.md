# Laboratoires

Ce dépôt contient la matière pour réaliser les laboratoires ainsi que la solution réalisée lors des laboratoires du cours `INF600E-Création de langages informatiques`.

# Répertoires

Chaque répertoire (`laboratoire0*`) est structuré de la manière suivante:

- grammar : Ce répertoire contient le fichier de grammaire de notre langage.
- src: L'ensemble du code source (note les fichiers générés par sablecc ne sont pas présents).
- start.sh : Fichier d'initialisation (supprime les répertoires de sablecc, exécute sablecc, compile le projet et l'exécute).
