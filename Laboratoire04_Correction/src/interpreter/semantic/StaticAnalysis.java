package interpreter.semantic;

import java.util.HashMap;
import java.util.Map;

import interpreter.ProgramException;
import interpreter.analysis.DepthFirstAdapter;
import interpreter.node.AAndExpr;
import interpreter.node.AAssignInstr;
import interpreter.node.ADivFactor;
import interpreter.node.AEqualComp;
import interpreter.node.AFalseTerm;
import interpreter.node.AGtComp;
import interpreter.node.AIfElseInstr;
import interpreter.node.AIfInstr;
import interpreter.node.AMinusArith;
import interpreter.node.AModFactor;
import interpreter.node.AMultFactor;
import interpreter.node.ANumTerm;
import interpreter.node.AOrExpr;
import interpreter.node.APlusArith;
import interpreter.node.APrintInstr;
import interpreter.node.AStringTerm;
import interpreter.node.ATrueTerm;
import interpreter.node.AVarTerm;
import interpreter.node.Node;
import interpreter.type.*;

public class StaticAnalysis extends DepthFirstAdapter {

	private Map<String, Type> variables = new HashMap<String, Type>();

	private Type result_type;

	private StaticAnalysis() {
	}

	public static void start(Node tree) {
		StaticAnalysis analysis = new StaticAnalysis();
		tree.apply(analysis);
	}

	@Override
	public void caseAAssignInstr(AAssignInstr node) {
		String ident = node.getIdent().getText();
		node.getExpr().apply(this);

		if (variables.containsKey(ident) && !(variables.get(ident).getClass().equals(result_type.getClass()))) {
			throw new ProgramException(node.getAssign(),
					"expected " + variables.get(ident).to_s() + " type expression: got " + result_type.to_s());
		} else {
			variables.put(ident, result_type);
		}
	}

	@Override
	public void caseAPrintInstr(APrintInstr node) {
		node.getExpr().apply(this);
	}

	@Override
	public void caseAIfInstr(AIfInstr node) {
		node.getExpr().apply(this);
		if (!(result_type instanceof BoolType)) {
			throw new ProgramException(node.getIf(), "expected Boolean expression: got " + node.getExpr());
		}

		node.getBlock().apply(this);
	}

	@Override
	public void caseAIfElseInstr(AIfElseInstr node) {
		node.getExpr().apply(this);
		if (!(result_type instanceof BoolType)) {
			throw new ProgramException(node.getIf(), "expected Boolean expression: got " + node.getExpr());
		}

		node.getThenBlock().apply(this);

		node.getElseBlock().apply(this);
	}

	@Override
	public void caseAStringTerm(AStringTerm node) {
		result_type = new StringType();
	}

	@Override
	public void caseANumTerm(ANumTerm node) {
		result_type = new IntType();
	}

	@Override
	public void caseATrueTerm(ATrueTerm node) {
		result_type = new BoolType();
	}

	@Override
	public void caseAFalseTerm(AFalseTerm node) {
		result_type = new BoolType();
	}

	@Override
	public void caseAVarTerm(AVarTerm node) {

		String variable_name = node.getIdent().getText();

		if (this.variables.containsKey(variable_name)) {
			this.result_type = this.variables.get(variable_name);
		} else {
			throw new ProgramException(node.getIdent(), "unknown variable name " + node.getIdent().getText());
		}
	}

	@Override
	public void caseAMultFactor(AMultFactor node) {
		node.getFactor().apply(this);
		Type left = this.result_type;

		node.getTerm().apply(this);
		Type right = this.result_type;

		this.result_type = left.mul(right, node.getMult());
	}

	@Override
	public void caseADivFactor(ADivFactor node) {

		node.getFactor().apply(this);
		Type left = this.result_type;

		node.getTerm().apply(this);
		Type right = this.result_type;

		this.result_type = left.div(right, node.getDiv());
	}

	@Override
	public void caseAModFactor(AModFactor node) {

		node.getFactor().apply(this);
		Type left = this.result_type;

		node.getTerm().apply(this);
		Type right = this.result_type;

		this.result_type = left.mod(right, node.getMod());
	}

	@Override
	public void caseAPlusArith(APlusArith node) {
		node.getFactor().apply(this);
		Type left = this.result_type;

		node.getArith().apply(this);
		Type right = this.result_type;

		this.result_type = left.add(right, node.getPlus());
	}

	@Override
	public void caseAMinusArith(AMinusArith node) {

		node.getFactor().apply(this);
		Type left = this.result_type;

		node.getArith().apply(this);
		Type right = this.result_type;

		this.result_type = left.sub(right, node.getMinus());
	}

	@Override
	public void caseAAndExpr(AAndExpr node) {
		node.getExpr().apply(this);
		Type left = this.result_type;

		node.getComp().apply(this);
		Type right = this.result_type;

		if (left instanceof BoolType && right instanceof BoolType) {
			result_type = new BoolType();
		} else {
			throw new ProgramException(node.getAnd(), "expected Boolean expression: got " + node.getAnd().getText());
		}
	}

	@Override
	public void caseAOrExpr(AOrExpr node) {
		node.getExpr().apply(this);
		Type left = this.result_type;

		node.getComp().apply(this);
		Type right = this.result_type;

		if (left instanceof BoolType && right instanceof BoolType) {
			result_type = new BoolType();
		} else {
			throw new ProgramException(node.getOr(), "expected Boolean expression: got " + node.getOr().getText());
		}
	}

	@Override
	public void caseAEqualComp(AEqualComp node) {
		node.getComp().apply(this);
		Type left = this.result_type;

		node.getArith().apply(this);
		Type right = this.result_type;

		this.result_type = left.equal(right, node.getEqual());
	}

	@Override
	public void caseAGtComp(AGtComp node) {
		node.getComp().apply(this);
		Type left = this.result_type;

		node.getArith().apply(this);
		Type right = this.result_type;

		this.result_type = left.greaterThan(right, node.getGt());
	}

}