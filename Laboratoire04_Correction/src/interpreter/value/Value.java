package interpreter.value;

import interpreter.ProgramException;
import interpreter.node.Token;

public class Value extends Object {
	public Value add(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for +:" + this.to_s() + " + " + value.to_s());
	};

	public Value sub(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for -:" + this.to_s() + " - " + value.to_s());
	};

	public Value mul(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for *:" + this.to_s() + " * " + value.to_s());
	};

	public Value div(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for /:" + this.to_s() + " / " + value.to_s());
	};

	public Value mod(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for %:" + this.to_s() + " % " + value.to_s());
	};

	public String to_s() {
		return null;
	}

	public Boolean greaterThan(Value value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for >:" + this.to_s() + " > " + value.to_s());
	}

	public Boolean equal(Value value, Token position) {
		if (this.getClass().equals(value.getClass())) {
			if (this == value) {
				return true;
			} else {
				return false;
			}
		}
		throw new ProgramException(position,
				"unsupported operand type(s) for ==:" + this.to_s() + " == " + value.to_s());
	}
}
