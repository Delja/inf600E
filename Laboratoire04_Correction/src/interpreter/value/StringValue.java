package interpreter.value;

import interpreter.ProgramException;
import interpreter.node.Token;

public class StringValue extends Value {
	private final String value;
	
	public StringValue(String value) {
		this.value = value;
	}

	@Override
	public String to_s() {
		return String.valueOf(getValue());
	}

	public String getValue() {
		return value;
	};
	
	@Override
	public Value add(Value value, Token position) {
		if (value instanceof StringValue || value instanceof IntValue) {
			return (new StringValue(this.to_s() + value.to_s()));
		}
		throw new ProgramException(position,
				"unsupported operand type(s) for +: " + this.to_s() + " + " + value.to_s());
	};
}
