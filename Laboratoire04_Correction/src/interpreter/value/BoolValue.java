package interpreter.value;

public class BoolValue extends Value {
	private final Boolean value;

	public BoolValue(Boolean value) {
		this.value = value;
	}

	@Override
	public String to_s() {
		return String.valueOf(getValue());
	}

	public Boolean getValue() {
		return value;
	};
}
