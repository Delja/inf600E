package interpreter.type;

import interpreter.ProgramException;
import interpreter.node.Token;

public class IntType extends Type {

	public IntType() {
	}

	@Override
	public Type add(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for +:" + this.to_s() + " + " + value.to_s());
	};

	@Override
	public Type sub(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for -:" + this.to_s() + " - " + value.to_s());
	};

	@Override
	public Type mul(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for *:" + this.to_s() + " * " + value.to_s());
	};

	@Override
	public Type div(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for /:" + this.to_s() + " / " + value.to_s());
	};

	@Override
	public Type mod(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for %:" + this.to_s() + " % " + value.to_s());
	};

	@Override
	public String to_s() {
		return "Int";
	}
}
