package interpreter.type;

public class BoolType extends Type {

	public BoolType() {
	}

	@Override
	public String to_s() {
		return "Bool";
	}
}
