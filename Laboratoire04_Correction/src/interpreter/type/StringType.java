package interpreter.type;

import interpreter.ProgramException;
import interpreter.node.Token;

public class StringType extends Type {

	public StringType() {
	}

	@Override
	public Type add(Type value, Token position) {
		if (value instanceof StringType) {
			return (new StringType());
		}
		throw new ProgramException(position,
				"unsupported operand type(s) for +: " + this.to_s() + " + " + value.to_s());
	};

	@Override
	public String to_s() {
		return "String";
	}
}
