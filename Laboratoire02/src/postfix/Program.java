
package postfix;

import postfix.analysis.*;
import postfix.node.*;
import postfix.value.*;

import java.util.HashMap;
import java.util.Map;

import postfix.*;

public class Program extends DepthFirstAdapter {

	private Value result;
	
	
	@Override
	public void caseANumberTerm(ANumberTerm node) {
		
	}
		
	@Override
	public void caseATrueTerm(ATrueTerm node) {
		
	}

	@Override
	public void caseAFalseTerm(AFalseTerm node) {
		
	}


	@Override
	public void caseAMultFactor(AMultFactor node) {

	}

	@Override
	public void caseADivFactor(ADivFactor node) {

	}

	
	@Override
	public void caseAModFactor(AModFactor node) {

	}
	
	@Override
	public void caseAPlusArith(APlusArith node) {
		
	}

	@Override
	public void caseAMinusArith(AMinusArith node) {
		
	}

	@Override
	public void caseAAssignInstr(AAssignInstr node) {
		
	}

	@Override
	public void caseAVarTerm(AVarTerm node) {
		
	}
	
	@Override
	public void caseAIfInstr(AIfInstr node) {
		
	}

	@Override
	public void caseAEqualComp(AEqualComp node) {
		
	}

	@Override
	public void caseAGtComp(AGtComp node) {
		
		
	}

	@Override
	public void caseAPrintInstr(APrintInstr node) {
		
	}



	private Program() {

	}

	public static void Start(Node tree) {

	}

	public boolean isOverflow(int left, int right, int result) {
		if (left > 0 && right > 0) {
			return result < 0;
		} else if (left < 0 && right < 0) {
			return result > 0;
		}
		return false;
	}

	public boolean isMultOverflow(int left, int right, int result) {
		if (left == 0 || right == 0) {
			return false;
		} else if (left == (result / right)) {
			return false;
		}
		return true;
	}
}
