
package postfix;

import java.io.*;

import postfix.lexer.*;
import postfix.node.*;
import postfix.parser.*;

public class Main {

	public static void main(String[] args) {

		System.out.println("Enter a program: ");

		Parser p = new Parser(new Lexer(new PushbackReader(new InputStreamReader(System.in), 1024)));

		Node tree;
		try {
			tree = p.parse();
			
			System.out.println();
			Program.Start(tree);
			
		} catch (ProgramException e) {
			System.err.println("CALCULATOR ERROR: " + e.getMessage());
			System.exit(1);
		} catch (ParserException e) {
			System.err.println("SYNTAX ERROR: " + e.getMessage());
			System.exit(1);
		} catch (LexerException e) {
			System.err.println("LEXICAL ERROR: " + e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			System.err.println("INPUT/OUTPUT ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

}
