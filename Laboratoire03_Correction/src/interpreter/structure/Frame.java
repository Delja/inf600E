package interpreter.structure;

import java.util.HashMap;

import interpreter.ProgramException;
import interpreter.node.Token;
import interpreter.value.*;

public class Frame {

	HashMap<String, Value> localVariable = new HashMap <String, Value>();
	
	Frame previousFrame;
	
	public Value returnValue;
	
	public Frame(Frame previousFrame) {
		this.previousFrame = previousFrame;
	}
	
	public void setVar(String name, Value value) {
		this.localVariable.put(name, value);
	}
	
	public Frame getPreviousFrame(){
		return this.previousFrame;
	}
	
	public Value getVar(Token name) {
		if (this.localVariable.containsKey(name.getText())) {
			return this.localVariable.get(name.getText());
		} else {
			throw new ProgramException(name, "unknown variable name " + name.getText());
		}
	}
}
