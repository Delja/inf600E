package interpreter.structure;

import java.util.HashMap;
import interpreter.node.*;
import interpreter.*;

public class FunctionTable {
	
	HashMap<String, ADeclaration> functions = new HashMap<String, ADeclaration>();
	
	public ADeclaration getFunction(Token name){
		if(! functions.containsKey(name.getText()))
			throw new ProgramException(name, "No procedure `" + name.getText() + "` is defined");
		return functions.get(name.getText());
	}
	
	public void setFunction(ADeclaration procedure) {
		if(functions.containsKey(procedure.getIdent().getText()))
			throw new ProgramException(procedure.getFun(), "A `" + procedure.getIdent().getText() + "` procedure is already defined");
		functions.put(procedure.getIdent().getText(), procedure);
	}
}
