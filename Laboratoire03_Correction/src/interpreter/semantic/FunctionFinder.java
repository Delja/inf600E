package interpreter.semantic;

import interpreter.analysis.DepthFirstAdapter;
import interpreter.node.ADeclaration;
import interpreter.node.Node;
import interpreter.structure.FunctionTable;

public class FunctionFinder extends DepthFirstAdapter {

	private FunctionTable fonctionTable;
	
	private FunctionFinder(FunctionTable functions) {
		this.fonctionTable = functions;
	}
	
	public static void startAnalyse(FunctionTable fonctionTable, Node tree) {
		FunctionFinder functionfinder = new FunctionFinder(fonctionTable);
		tree.apply(functionfinder);
	}
	
	@Override
	public void caseADeclaration(ADeclaration node) {
		fonctionTable.setFunction(node);
	}
}
