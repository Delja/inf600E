package interpreter.semantic;

import interpreter.analysis.DepthFirstAdapter;
import interpreter.node.ACall;
import interpreter.node.Node;
import interpreter.structure.FunctionTable;

public class CallVerifier extends DepthFirstAdapter {

	private FunctionTable fonctionTable;
	
	private CallVerifier(FunctionTable functions) {
		this.fonctionTable = functions;
	}
	
	public static void startAnalyse(FunctionTable fonctionTable, Node tree) {
		CallVerifier callVerifier = new CallVerifier(fonctionTable);
		tree.apply(callVerifier);
	}
	
	@Override
	public void caseACall(ACall node) {
		fonctionTable.getFunction(node.getIdent());
	}
}