
package interpreter;

import java.util.HashMap;
import java.util.Map;

import interpreter.analysis.*;
import interpreter.node.*;
import interpreter.structure.Frame;
import interpreter.structure.FunctionTable;
import interpreter.value.*;

public class Program extends DepthFirstAdapter {

	private Value result;

	private HashMap<Integer, IntValue> int_objects = new HashMap<Integer, IntValue>();

	BoolValue true_value = new BoolValue(true);
	BoolValue false_value = new BoolValue(false);

	private Frame currentFrame;

	private FunctionTable fonctionTable;

	private Program(FunctionTable functions) {
		this.fonctionTable = functions;
		this.currentFrame = new Frame(null);
	}

	public static void start(FunctionTable fonctionTable, Node tree) {
		Program program = new Program(fonctionTable);
		tree.apply(program);
	}

	/*
	 * 
	 * Helpers section
	 * 
	 */

	private BoolValue checkReturnComp(Token position, Value objet) {
		if (objet instanceof BoolValue) {
			return ((BoolValue) objet);
		} else {
			throw new ProgramException(position, "expected Boolean expression: got " + position.getText());
		}
	}

	private Value checkExistValue(Value value) {
		if (value instanceof IntValue) {
			if (int_objects.containsKey(((IntValue) value).getValue())) {
				return int_objects.get(((IntValue) value).getValue());
			}
		}
		return value;
	}

	private boolean isOverflow(int left, int right, int result) {
		if (left > 0 && right > 0) {
			return result < 0;
		} else if (left < 0 && right < 0) {
			return result > 0;
		}
		return false;
	}

	private boolean isMultOverflow(int left, int right, int result) {
		if (left == 0 || right == 0) {
			return false;
		} else if (left == (result / right)) {
			return false;
		}
		return true;
	}

	/*
	 * 
	 * Redefinition of all cases productions
	 * 
	 */

	@Override
	public void caseAAssignInstr(AAssignInstr node) {
		String ident = node.getIdent().getText();
		node.getExpr().apply(this);
		this.currentFrame.setVar(ident, result);
	}

	@Override
	public void caseAReturnInstr(AReturnInstr node) {
		if (node.getExpr() != null) {
			node.getExpr().apply(this);
			this.currentFrame.returnValue = this.result;
		} else {
			this.currentFrame.returnValue = null;
		}
		throw new ReturnException();
	}

	@Override
	public void caseADeclaration(ADeclaration node) {
	}

	@Override
	public void caseACall(ACall node) {
		// Get the procedure from the fonctionTable
		ADeclaration procedure = fonctionTable.getFunction(node.getIdent());
		// Evaluate the call argument
		node.getExpr().apply(this);
		// Create a new frame
		Frame frame = new Frame(this.currentFrame);
		this.currentFrame = frame;
		// Set the parameter as a local variable
		this.currentFrame.setVar(procedure.getParam().getText(), this.result);

		try {
			procedure.getBlock().apply(this);
		} catch (ReturnException e) {
		}
		// Set the result with the return of the procedure
		this.result = this.currentFrame.returnValue;
		// Unstack the frame
		this.currentFrame = frame.getPreviousFrame();
	}

	@Override
	public void caseAPrintInstr(APrintInstr node) {
		node.getExpr().apply(this);
		System.out.println(result.to_s());
	}

	@Override
	public void caseAIfInstr(AIfInstr node) {
		node.getExpr().apply(this);
		if (!(result instanceof BoolValue)) {
			throw new ProgramException(node.getIf(), "expected Boolean expression: got " + node.getExpr());
		}

		if (((BoolValue) result).getValue()) {
			node.getBlock().apply(this);
		}
	}

	@Override
	public void caseAIfElseInstr(AIfElseInstr node) {
		node.getExpr().apply(this);
		if (!(result instanceof BoolValue)) {
			throw new ProgramException(node.getIf(), "expected Boolean expression: got " + node.getExpr());
		}

		if (((BoolValue) result).getValue()) {
			node.getThenBlock().apply(this);
		} else {
			node.getElseBlock().apply(this);
		}
	}

	@Override
	public void caseANumTerm(ANumTerm node) {
		try {
			Integer value = Integer.parseInt(node.getNumber().getText());
			if (!this.int_objects.containsKey(value)) {
				this.result = new IntValue(value);
				int_objects.put(value, (IntValue) this.result);
			} else {
				this.result = int_objects.get(value);
			}
		} catch (NumberFormatException e) {
			throw new ProgramException(node.getNumber(), "invalid number " + node.getNumber().getText());
		}
	}

	@Override
	public void caseATrueTerm(ATrueTerm node) {
		this.result = true_value;
	}

	@Override
	public void caseAFalseTerm(AFalseTerm node) {
		this.result = false_value;
	}

	@Override
	public void caseAVarTerm(AVarTerm node) {
		this.result = this.currentFrame.getVar(node.getIdent());
	}

	@Override
	public void caseAMultFactor(AMultFactor node) {
		node.getFactor().apply(this);
		Value left = this.result;

		node.getTerm().apply(this);
		Value right = this.result;

		this.result = checkExistValue(left.mul(right, node.getMult()));

		if (isMultOverflow(((IntValue) left).getValue(), ((IntValue) right).getValue(),
				((IntValue) result).getValue())) {
			throw new ProgramException(node.getMult(),
					"overflow of the operation result: " + left.to_s() + " * " + right.to_s());
		}
	}

	@Override
	public void caseADivFactor(ADivFactor node) {

		node.getFactor().apply(this);
		Value left = this.result;

		node.getTerm().apply(this);
		Value right = this.result;

		this.result = checkExistValue(left.div(right, node.getDiv()));
	}

	@Override
	public void caseAModFactor(AModFactor node) {

		node.getFactor().apply(this);
		Value left = this.result;

		node.getTerm().apply(this);
		Value right = this.result;

		this.result = left.mod(right, node.getMod());
	}

	@Override
	public void caseAPlusArith(APlusArith node) {
		node.getArith().apply(this);
		Value left = this.result;

		node.getFactor().apply(this);
		Value right = this.result;

		this.result = checkExistValue(left.add(right, node.getPlus()));

		if (isOverflow(((IntValue) left).getValue(), ((IntValue) right).getValue(), ((IntValue) result).getValue())) {
			throw new ProgramException(node.getPlus(), "overflow of the operation result: " + left + " + " + right);
		}
	}

	@Override
	public void caseAMinusArith(AMinusArith node) {

		node.getArith().apply(this);
		Value left = this.result;

		node.getFactor().apply(this);
		Value right = this.result;

		this.result = checkExistValue(left.sub(right, node.getMinus()));

		// Overflow if the arguments have different signs and the sign of the result is
		// different than the sign of left
		boolean sleft = ((IntValue) left).getValue() > 0; // get the sign of left
		boolean sright = ((IntValue) right).getValue() > 0; // get the sign of right
		boolean sresult = ((IntValue) result).getValue() > 0; // get the sign of result

		if ((sleft != sright) && (sresult != sleft)) {
			throw new ProgramException(node.getMinus(), "overflow of the operation result: " + left + " - " + right);
		}
	}

	@Override
	public void caseAAndExpr(AAndExpr node) {
		node.getExpr().apply(this);
		BoolValue left = checkReturnComp(node.getAnd(), this.result);

		node.getComp().apply(this);
		BoolValue right = checkReturnComp(node.getAnd(), this.result);

		if (left.getValue() && right.getValue()) {
			this.result = true_value;
		} else {
			this.result = false_value;
		}
	}

	@Override
	public void caseAOrExpr(AOrExpr node) {
		node.getExpr().apply(this);
		BoolValue left = checkReturnComp(node.getOr(), this.result);

		node.getComp().apply(this);
		BoolValue right = checkReturnComp(node.getOr(), this.result);

		if (left.getValue() || right.getValue()) {
			this.result = true_value;
		} else {
			this.result = false_value;
		}
	}

	@Override
	public void caseAEqualComp(AEqualComp node) {
		node.getComp().apply(this);
		Value left = this.result;

		node.getArith().apply(this);
		Value right = this.result;

		if (left.equal(right, node.getEqual())) {
			this.result = true_value;
		} else {
			this.result = false_value;
		}
	}

	@Override
	public void caseAGtComp(AGtComp node) {
		node.getComp().apply(this);
		Value left = this.result;

		node.getArith().apply(this);
		Value right = this.result;

		if (left.greaterThan(right, node.getGt())) {
			this.result = true_value;
		} else {
			this.result = false_value;
		}
	}
}
