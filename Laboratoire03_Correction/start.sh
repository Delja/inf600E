#!/bin/bash

rm -rf src/interpreter/analysis/ src/interpreter/lexer/ src/interpreter/node/ src/interpreter/parser/
java -jar ../sablecc.jar -d src grammar/interpreter.sablecc
javac -cp src -d bin src/interpreter/Main.java
java -cp bin interpreter.Main