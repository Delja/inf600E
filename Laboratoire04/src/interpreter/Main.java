
package interpreter;

import java.io.*;

import interpreter.lexer.*;
import interpreter.node.*;
import interpreter.parser.*;

public class Main {

	public static void main(String[] args) {

		System.out.println("Enter a source code: ");

		Parser p = new Parser(new Lexer(new PushbackReader(new InputStreamReader(System.in), 1024)));

		Node tree;
		try {
			tree = p.parse();
			// Program
			System.out.println();
			Program.start(tree);
		} catch (ProgramException e) {
			System.err.println("PROGRAM ERROR: " + e.getMessage());
			System.exit(1);
		} catch (ParserException e) {
			System.err.println("SYNTAX ERROR: " + e.getMessage());
			System.exit(1);
		} catch (LexerException e) {
			System.err.println("LEXICAL ERROR: " + e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			System.err.println("INPUT/OUTPUT ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

}
