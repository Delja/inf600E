package interpreter.type;

public class BoolType extends Type {

	public BoolType() {}
	
	public String to_s() {
		return "Bool";
	}
}
