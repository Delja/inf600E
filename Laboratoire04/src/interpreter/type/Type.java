package interpreter.type;

import interpreter.ProgramException;
import interpreter.node.Token;

public class Type extends Object {
	public Type add(Type value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for +:" + this.to_s() + " + " + value.to_s());
	};

	public Type sub(Type value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for -:" + this.to_s() + " - " + value.to_s());
	};

	public Type mul(Type value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for *:" + this.to_s() + " * " + value.to_s());
	};

	public Type div(Type value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for /:" + this.to_s() + " / " + value.to_s());
	};

	public Type mod(Type value, Token position) {
		throw new ProgramException(position, "unsupported operand type(s) for %:" + this.to_s() + " % " + value.to_s());
	};

	public String to_s() {
		return null;
	}

	public BoolType greaterThan(Type value, Token position) {
		if (this.getClass().equals(value.getClass())) {
			return new BoolType();
		}
		throw new ProgramException(position, "unsupported operand type(s) for >:" + this.to_s() + " > " + value.to_s());
	}
	
	public BoolType equal(Type value, Token position) {
		if (this.getClass().equals(value.getClass())) {
			return new BoolType();
		}
		throw new ProgramException(position, "unsupported operand type(s) for ==:" + this.to_s() + " == " + value.to_s());
	}
}
