package interpreter.type;

import interpreter.ProgramException;
import interpreter.node.Token;

public class IntType extends Type {

	public IntType() {
    }

	public Type add(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for +:" + this.to_s() + " + " + value.to_s());
	};

	public Type sub(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for -:" + this.to_s() + " - " + value.to_s());
	};

	public Type mul(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for *:" + this.to_s() + " * " + value.to_s());
	};

	public Type div(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for /:" + this.to_s() + " / " + value.to_s());
	};

	public Type mod(Type value, Token position) {
		if (value instanceof IntType) {
			return (new IntType());
		}
		throw new ProgramException(position, "unsupported operand type(s) for %:" + this.to_s() + " % " + value.to_s());
	};
	
	public String to_s() {
		return "Int";
	}
}
