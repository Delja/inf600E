#!/bin/bash

rm -rf src/postfix/analysis/ src/postfix/lexer/ src/postfix/node/ src/postfix/parser/
java -jar ../sablecc.jar -d src grammar/postfix.sablecc
javac -cp src -d bin src/postfix/Main.java
java -cp bin postfix.Main